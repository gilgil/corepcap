#include "widget.h"
#include "ui_widget.h"
#include <QScrollBar>
#include "gtrace.h"

Widget::Widget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::Widget)
{
	ui->setupUi(this);

	QObject::connect(&sniffThread_, &SniffThread::captured, this, &Widget::processCaptured, Qt::AutoConnection);
	QObject::connect(&sniffThread_, &SniffThread::captureFinished, this, &Widget::processCaptureFinished, Qt::AutoConnection);
	sniffThread_.start();
}

Widget::~Widget()
{
	GTRACE("~Widget beg");
	delete ui;
	GTRACE("~Widget end");
}

void Widget::processCaptured(quint32 size) {
	QString msg = QString("%1 bytes captured").arg(size);
	ui->plainTextEdit->insertPlainText(msg + "\n");
	QScrollBar *sb = ui->plainTextEdit->verticalScrollBar();
	sb->setValue(sb->maximum());
	GTRACE("%s", qPrintable(msg));
}

void Widget::processCaptureFinished() {
	GTRACE("bef call close()");
	close();
	GTRACE("aft call close()");
}

void Widget::on_pbClose_clicked() {
	close();
}
