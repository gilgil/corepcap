#pragma once

#include <QObject>
#include <QThread>
#include "corepcap.h"

struct SniffThread : QThread {
	Q_OBJECT

public:
	struct Param {
		QString dev_{"wlan0"};
		QString pw_;
		QString filter_;

		bool parse(int argc, char** argv);
		static void usage();
	} param_;

public:
	SniffThread() {}
	~SniffThread() override { close(); }
	bool active_{false};

protected:
	CorePcap cp_;
	pcap_t* pcap_{nullptr};
	bool open();
	bool close();

protected:
	void run() override;

signals:
	void captured(quint32 size);
	void captureFinished();
};
