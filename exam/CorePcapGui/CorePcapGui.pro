QT += core gui widgets
CONFIG += c++11
DEFINES += QT_DEPRECATED_WARNINGS
SOURCES += \
	../../corepcap.cpp \
	../../gtrace.cpp \
	corepcapgui.cpp \
	sniffthread.cpp \
    widget.cpp

HEADERS += \
	../../corepcap.h \
	../../gtrace.h \
	sniffthread.h \
    widget.h

FORMS += \
    widget.ui

INCLUDEPATH += ../..
LIBS += -lpcap
DESTDIR = $${PWD}/../../bin

PRE_TARGETDEPS *= $${PWD}/../../bin/corepcap
android {
	deployment.files += $${PWD}/../../bin/corepcap
	deployment.path = /assets
	INSTALLS += deployment
}
