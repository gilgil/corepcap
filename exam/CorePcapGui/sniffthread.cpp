#include "sniffthread.h"
#include "gtrace.h"

bool SniffThread::Param::parse(int argc, char** argv) {
	if (argc >= 2)
		dev_ = argv[1];
	if (argc >= 3)
		pw_ = argv[2];
	if (argc >= 4)
		filter_ = argv[3];
	return true;
}

void SniffThread::Param::usage() {
	printf("syntax: CorePcapGui [<device> [<password> [<filter>]]]\n");
	printf("sample: CorePcapGui wlan0 mypassword 'tcp port 80'\n");
}

bool SniffThread::open() {
	QString cmd;
#ifdef Q_OS_ANDROID
	cmd = QString("su -c ./corepcap dev %1").arg(param_.dev_);
#else
	if (param_.pw_ == "")
		cmd = QString("sudo ./corepcap dev %1").arg(param_.dev_);
	else {
		cmd = QString("echo %1 | sudo -S ./corepcap dev %2").arg(param_.pw_).arg(param_.dev_);
		cp_.pw_ = qPrintable(param_.pw_);
	}
#endif // Q_OS_ANDROID
	if (param_.filter_ != "")
		cmd += QString(" -f '%1'").arg(param_.filter_);
	cmd += QString(" file -");

	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_ = cp_.openCmd(qPrintable(cmd), errbuf);
	if (pcap_ == nullptr)
		return false;
	active_ = true;
	return true;
}

bool SniffThread::close() {
	GTRACE("beg");
	active_ = false;

	cp_.close();

	quit();
	wait();
	GTRACE("end");
	return true;
}

void SniffThread::run() {
	GTRACE("beg");
	if (!open()) {
		emit captureFinished();
		return;
	}
	Q_ASSERT(pcap_ != nullptr);
	while (active_) {
		struct pcap_pkthdr* header;
		const u_char* packet;
		int res = pcap_next_ex(pcap_, &header, &packet);
		if (res == 0) continue;
		if (res == -1 || res == -2) {
			GTRACE("pcap_next_ex return %d", res);
			break;
		}
		GTRACE("%d bytes captured", header->caplen);
		emit captured(header->caplen);
	}
	GTRACE("bef call emit closed");
	emit captureFinished();
	GTRACE("aft call emit closed");
	GTRACE("end");
}
