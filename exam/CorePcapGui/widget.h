#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "sniffthread.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
	Q_OBJECT

public:
	Widget(QWidget *parent = nullptr);
	~Widget();

	SniffThread sniffThread_;

public slots:
	void processCaptured(quint32 size);
	void processCaptureFinished();

private slots:

	void on_pbClose_clicked();

private:
	Ui::Widget *ui;
};
#endif // WIDGET_H
