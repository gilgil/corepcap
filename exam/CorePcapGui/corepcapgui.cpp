#include <QApplication>
#include <QFile>
#include "widget.h"
#include "gtrace.h"

#ifdef Q_OS_ANDROID
bool copyFileFromAssets(QString fileName, QFile::Permissions permissions) {
	QString sourceFileName = QString("assets:/") + fileName;
	QFile sFile(sourceFileName);
	QFile dFile(fileName);
	if (!dFile.exists()) {
		if (!sFile.exists()) {
			QString msg = QString("src file(%1) not exists").arg(sourceFileName);
			GTRACE("%s", qPrintable(msg));
			return false;
		}

		sFile.copy(fileName);
		QFile::setPermissions(fileName, permissions);
		GTRACE("copy file(%s) succeed", qPrintable(fileName));
	}
	return true;
}
#endif // Q_OS_ANDROID

int main(int argc, char *argv[]) {
	GTRACE("application beg");
	QApplication a(argc, argv);

#ifdef Q_OS_ANDROID
	copyFileFromAssets("corepcap", QFile::ReadOwner | QFile::WriteOwner | QFile::ExeOwner);
#endif // Q_OS_ANDROID

	Widget* w = new Widget;
	w->sniffThread_.param_.usage();
	w->sniffThread_.param_.parse(argc, argv);
	w->show();
	int res = a.exec();
	GTRACE("bef delete w");
	delete w;
	GTRACE("aft delete w");
	GTRACE("application terminated successfully");
	return res;
}
