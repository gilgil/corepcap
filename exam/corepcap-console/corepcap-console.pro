TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
INCLUDEPATH += ../..
LIBS += -lpcap
DESTDIR = $${PWD}/../../bin
PRE_TARGETDEPS *= $${PWD}/../../bin/corepcap
SOURCES += \
	../../corepcap.cpp \
	../../gtrace.cpp \
	corepcap-console.cpp

HEADERS += \
	../../corepcap.h \
	../../gtrace.h
