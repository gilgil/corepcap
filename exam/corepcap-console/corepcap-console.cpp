#include <corepcap.h>
#include <stdio.h>

struct Param {
	std::string dev_;
	std::string pw_;
	std::string filter_;

	bool parse(int argc, char** argv) {
		if (argc < 2) {
			usage();
			return false;
		}
		dev_ = argv[1];
		if (argc >= 3)
			pw_ = argv[2];
		if (argc >= 4)
			filter_ = argv[3];
		return true;
	}

	static void usage() {
		printf("syntax: corepcap-console <device> [<password> [<filter>]]\n");
		printf("sample: corepcap-console wlan0 mypassword 'tcp port 80'\n");
	}
};

int main(int argc, char* argv[]) {
	Param param;
	if (!param.parse(argc, argv))
		return -1;

	EasyCorePcap ecp;
	ecp.process_ = "./corepcap";
	if (param.filter_ != "") ecp.filter_ = param.filter_;
	if (param.pw_ != "") ecp.pw_ = param.pw_;

	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* pcap = ecp.openLive(param.dev_.c_str(), BUFSIZ, 1, 1, errbuf);
	if (pcap == nullptr) {
		fprintf(stderr, "ecp.openLive return null - %s\n", errbuf);
		return -1;
	}

	while (true) {
		struct pcap_pkthdr* header;
		const u_char* packet;
		int res = pcap_next_ex(pcap, &header, &packet);
		if (res == 0) continue;
		if (res == -1 || res == -2) {
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(pcap));
			break;
		}
		printf("%u bytes captured\n", header->caplen);
	}

	ecp.close();
}
