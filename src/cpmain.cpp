#include <csignal>
#include "cpcorepcap.h"

LCorePcap cp;

void sighandler(int signo) {
	const char* msg = "unknown";
	switch (signo) {
		case SIGINT: msg = "SIGINT"; break;
		case SIGILL: msg = "SIGILL"; break;
		case SIGABRT: msg = "SIGABRT"; break;
		case SIGFPE: msg = "SIGFPE"; break;
		case SIGSEGV: msg = "SIGSEGV"; break;
		case SIGTERM: msg = "SIGTERM"; break;
		case SIGHUP: msg = "SIGHUP"; break;
		case SIGQUIT: msg = "SIGQUIT"; break;
		case SIGTRAP: msg = "SIGTRAP"; break;
		case SIGKILL: msg = "SIGKILL"; break;
		case SIGBUS: msg = "SIGBUS"; break;
		case SIGSYS: msg = "SIGSYS"; break;
		case SIGPIPE: msg = "SIGPIPE"; break;
		case SIGALRM: msg = "SIGALRM"; break;
		case SIGURG: msg = "SIGURG"; break;
		case SIGSTOP: msg = "SIGSTOP"; break;
		case SIGTSTP: msg = "SIGTSTP"; break;
		case SIGCONT: msg = "SIGCONT"; break;
		case SIGCHLD: msg = "SIGCHLD"; break;
		case SIGTTIN: msg = "SIGTTIN"; break;
		case SIGTTOU: msg = "SIGTTOU"; break;
		case SIGPOLL: msg = "SIGPOLL"; break;
		case SIGXCPU: msg = "SIGXCPU"; break;
		case SIGXFSZ: msg = "SIGXFSZ"; break;
		case SIGVTALRM: msg = "SIGVTALRM"; break;
		case SIGPROF: msg = "SIGPROF"; break;
		case SIGUSR1: msg = "SIGUSR1"; break;
		case SIGUSR2: msg = "SIGUSR2"; break;
	}
	GTRACE("%s(%d)", msg, signo);
	if (cp.active())
		cp.close();
}

void prepareSignal() {
	signal(SIGINT, sighandler);
	signal(SIGILL, sighandler);
	signal(SIGABRT, sighandler);
	signal(SIGFPE, sighandler);
	signal(SIGSEGV, sighandler);
	signal(SIGTERM, sighandler);
	signal(SIGHUP, sighandler);
	signal(SIGQUIT, sighandler);
	signal(SIGTRAP, sighandler);
	signal(SIGKILL, sighandler);
	signal(SIGBUS, sighandler);
	signal(SIGSYS, sighandler);
	signal(SIGPIPE, sighandler);
	signal(SIGALRM, sighandler);
	signal(SIGURG, sighandler);
	signal(SIGSTOP, sighandler);
	signal(SIGTSTP, sighandler);
	signal(SIGCONT, sighandler);
	signal(SIGCHLD, sighandler);
	signal(SIGTTIN, sighandler);
	signal(SIGTTOU, sighandler);
	signal(SIGPOLL, sighandler);
	signal(SIGXCPU, sighandler);
	signal(SIGXFSZ, sighandler);
	signal(SIGVTALRM, sighandler);
	signal(SIGPROF, sighandler);
	signal(SIGUSR1, sighandler);
	signal(SIGUSR2, sighandler);
}

int main(int argc, char* argv[]) {
	gtrace_default("127.0.0.1", 8908, false, nullptr);
	GTRACE("corepcap started %s %s", __DATE__, __TIME__);
	prepareSignal();
	if (!cp.parse(argc, argv)) {
		fprintf(stderr, "%s\n", cp.error_.c_str());
		GTRACE("cp.parse return false %s", cp.error_.c_str());
		return -1;
	}
	if (!cp.open())
		return -1;
	cp.run();
	if (cp.active())
		cp.close();
	GTRACE("corepcap terminated successfully");
}
