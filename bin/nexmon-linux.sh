#!/bin/bash
if [ -z "$1" ]; then
        echo "syntax : nexmon-linux.sh <interface>"
        echo "sample : nexmon-linux.sh wlan1mon"
        echo ""
        echo "create virtual wireless adapter in linux:"
        echo "sudo modprobe mac80211_hwsim"
        echo "sudo gmon wlan1 wlan1mon"
        exit 1
fi

INTERFACE="$1"
echo "[nexmon-linux.sh] run corepcap $INTERFACE"
adb exec-out "su -c corepcap-nexmon.sh dev wlan0 file - 2>/dev/null" | corepcap file - dev $INTERFACE
echo ""
echo "[nexmon-linux.sh] run pkill corepcap"
adb exec-out "su -c pkill corepcap"
echo ""
echo "[nexmon-linux.sh] terminated successfully"
echo ""
