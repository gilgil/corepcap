TARGET=bin/corepcap
SRCS=$(wildcard src/*.cpp) $(wildcard gtrace.cpp)
OBJS=$(SRCS:%.cpp=%.o)

CPPFLAGS+=-g -O2
CPPFLAGS+=-I.
LDLIBS+=-lpcap -pthread

all: $(TARGET)

$(TARGET): $(OBJS)
	$(LINK.cpp) $^ $(LOADLIBES) $(LDLIBS) -o $@

clean:
	rm -f $(TARGET) $(OBJS)

install:
	sudo cp $(TARGET) /usr/sbin

uninstall:
	sudo rm -f /usr/sbin/corepcap

android-install:
	adb push $(TARGET) bin/corepcap-nexmon.sh /data/local/tmp
	adb exec-out "su -c 'mount -o rw,remount /system'"
	adb exec-out "su -c 'cp /data/local/tmp/corepcap /data/local/tmp/corepcap-nexmon.sh /system/xbin/'"
	adb exec-out "su -c 'chmod 755 /system/xbin/corepcap /system/xbin/corepcap-nexmon.sh'"
	adb exec-out "su -c 'mount -o ro,remount /system'"
	adb exec-out "rm /data/local/tmp/corepcap /data/local/tmp/corepcap-nexmon.sh"

android-uninstall:
	adb exec-out "su -c 'mount -o rw,remount /system'"
	adb exec-out "su -c 'rm -f /system/xbin/corepcap /system/xbin/corepcap-nexmon.sh'"
	adb exec-out "su -c 'mount -o ro,remount /system'"

test:
	@echo $(SRCS)
	@echo $(OBJS)
