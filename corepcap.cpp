#include "corepcap.h"
#include "gtrace.h"

pcap_t* CorePcap::openCmd(const char* cmd, char* errbuf) {
	GTRACE("cmd=%s", cmd);
	fp_ = popen(cmd, "r");
	if (fp_ == nullptr) {
		GTRACE("popen(%s) return null", cmd);
		return nullptr;
	}

	pcap_ = pcap_fopen_offline(fp_, errbuf);
	if (pcap_ == nullptr) {
		GTRACE("pcap_fopen_offline return null - %s", errbuf);
		return nullptr;
	}

	return pcap_;
}

void CorePcap::close() {
	if (fp_ != nullptr) {
		std::string cmd;
#ifdef __ANDROID__
		cmd = "su -c pkill corepcap";
#else
		if (pw_ != "")
			cmd = "echo " + pw_ + " | sudo -S pkill corepcap";
		else
			cmd = "sudo pkill corepcap";
#endif // __ANDROID__

		GTRACE("bef call %s", cmd.c_str());
		int res = system(cmd.c_str());
		GTRACE("system(%s) return %d", cmd.c_str(), res);

		GTRACE("bef call pclose(%p)", fp_);
		pclose(fp_);
		GTRACE("aft call pclose");
		fp_ = nullptr;
	}

	if (pcap_ != nullptr) {
		// ----- by gilgil 2020.10.22 -----
		// do not call pcap_close
		// pcap_close(cp->pcap_);
		// --------------------------------
		pcap_ = nullptr;
	}
}

pcap_t* EasyCorePcap::openLive(const char* dev, int snapLen, int promisc, int readTimeout, int waitTimeout, char* errbuf) {
	std::string cmd;
#ifdef __ANDROID__
	cmd = "su -c " + process_;
#else
	if (pw_ != "")
		cmd = "echo " + pw_ + " | sudo -S " + process_;
	else
		cmd = "sudo " + process_;
#endif // __ANDROID__

	cmd += std::string(" dev ") + dev +
			" -l " + std::to_string(snapLen) +
			" -p " + std::to_string(promisc) +
			" -rt " + std::to_string(readTimeout) +
			" -wt " + std::to_string(waitTimeout);

	if (filter_ != "")
		cmd += " -filter '" + filter_ + "'";

	cmd += " file -";
	return openCmd(cmd.c_str(), errbuf);
}
