#pragma once

#include <string>
#include <pcap.h>

struct CorePcap {
protected:
	FILE* fp_{nullptr};
	pcap_t* pcap_{nullptr};

public:
	CorePcap() {}
	virtual ~CorePcap() { close(); }

	std::string pw_;

	pcap_t* openCmd(const char* cmd, char* errbuf);
	void close();
};

struct EasyCorePcap: CorePcap {

	std::string filter_;
	std::string process_{"corepcap"};

	pcap_t* openLive(const char* dev, int snapLen, int promisc, int readTimeout, int waitTimeout, char* errbuf);
};
